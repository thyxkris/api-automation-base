package infrastructure;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class KBaseAPI<T extends KBaseContext> {

    protected T scenarioContext;

    public Logger getLogger() {
        return scenarioContext.getLogger();
    }


    public String getEndpoint() {
        return scenarioContext.getEndpoint();
    }

    public String setEndpoint(String endpoint) {
        return scenarioContext.setEndpoint(endpoint);
    }

    public String getContentType() {
        return scenarioContext.getContentType();
    }

    public String setContentType(String contentType) {
        return scenarioContext.setContentType(contentType);
    }

    public Response getResponse() {
        return scenarioContext.getResponse();
    }

    public Response setResponse(Response response) {
        return scenarioContext.setResponse(response);
    }


    public RequestSpecification getRequest() {
        return scenarioContext.getRequest();
    }

    public RequestSpecification setRequest(RequestSpecification request) {
        return scenarioContext.setRequest(request);
    }


    public KBaseAPI(T scenarioContext, String endpoint, String contentType) {
        this.scenarioContext = scenarioContext;
        this.setEndpoint(endpoint);
        this.setContentType(contentType);
        this.setRequest(given().contentType(this.getContentType()));


    }

}
