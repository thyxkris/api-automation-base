package infrastructure;


import org.apache.logging.log4j.Logger;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class KBaseStep<T extends KBaseContext> {
    //below no need to modify
    protected Logger logger;
    protected T scenarioContext;

    public KBaseStep(T scenarioContext) throws Throwable {
        this.scenarioContext = scenarioContext;
        logger = scenarioContext.getLogger();
        logger.info(registerClass(this.getClass().getName()));
    }

    public <TAPI extends KBaseAPI> KBaseAPI registerClass(String classToRegister) throws Exception {
        logger.info("registerClass; " + this.getClass() + "start setup ");
        if (classToRegister.equals("steps.CommonStep")) {
            return null;
        }
        classToRegister = classToRegister.replace("steps.", "apis.").replace("Step", "API");

        Class<?> c = Class.forName(classToRegister);
        Constructor<?> cons = c.getConstructor(this.scenarioContext.getClass());

        for (Object field : this.getClass().getSuperclass().getDeclaredFields()) {
            if (((Field) field).getType().equals(Class.forName(classToRegister))) {
                ((Field) field).setAccessible(true);
                ((Field) field).set(this, cons.newInstance(scenarioContext));
                return scenarioContext.setCurrentAPI((TAPI) ((Field) field).get(this));
            }
        }

        return null;
    }


}
