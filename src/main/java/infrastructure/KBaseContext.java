package infrastructure;

import cucumber.api.Scenario;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Kris Ma on 21/05/2017.
 */
public abstract class KBaseContext extends HashMap<String, Object> {

    //private Dictionary hashMap = new Hashtable();
    private HashMap<String, Object> hashMap = new HashMap<String, Object>();


    @Override
    public int size() {
        return this.hashMap.size();
    }

    @Override
    public boolean isEmpty() {
        return this.hashMap.isEmpty();
    }

    @Override
    public Object get(Object key) {
        return hashMap.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return this.hashMap.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return this.hashMap.remove(key);
    }

    private Scenario scenario;

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    protected String threadId;
    protected static int count = 0;
    protected Logger logger;

    public KBaseContext resetTestData() {
        for (String s : this.keySet()) {
            this.remove(s);
        }
        return this;
    }

    public Locale getLocale() {
        return (Locale) this.get("locale");
    }

    public Locale setLocale(Locale locale) {
        this.put("locale", locale);
        return (Locale) this.get("locale");
    }

    public Logger getLogger() {
        if (null == this.logger) {
            this.logger = LogManager.getLogger(getThreadId());
        }
        return logger;
    }

    public Logger getLogger(String name) {
        //if (null == this.logger||!this.logger.getName().equals(name)) {
        this.logger = LogManager.getLogger(name);
        //}
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public String getThreadId() {
        if (this.threadId == null) {
            threadId = Long.toString(Thread.currentThread().getId());//+ "count :" + Integer.toString(count++));
        }
        count++;
        return "threadId" + " " + Integer.toString(count);
    }

    public void dispose() {

        resetTestData();

    }


    //here we have to override the get and setCurrentAPI()
    public <T extends KBaseAPI> T getCurrentAPI() {
        return (T) this.get("currentAPI");
    }

    public <T extends KBaseAPI> T setCurrentAPI(T currentAPI) {
        this.put("currentAPI", currentAPI);
        return (T) this.get("currentAPI");
    }


    //below is totally for API testing
    //XSRF-TOKEN
    public String getXSRFTOKEN() {
        return (String) this.get("XSRF-TOKEN");
    }

    public String setXSRFTOKEN(String XSRFTOKEN) {
        this.put("XSRF-TOKEN", XSRFTOKEN);
        return (String) this.get("XSRF-TOKEN");
    }

    //tokenId
    public String getTokenId() {
        return (String) this.get("tokenId");
    }

    public String setTokenId(String tokenId) {
        this.put("tokenId", tokenId);
        return (String) this.get("tokenId");
    }

    //sessionId
    public String getSessionId() {
        return (String) this.get("tokenId");
    }

    public String setSessionId(String sessionId) {
        this.put("sessionId", sessionId);
        return (String) this.get("sessionId");
    }

    //end point
    public String getEndpoint() {
        return (String) this.get("endpoint");
    }

    public String setEndpoint(String endpoint) {
        this.put("endpoint", endpoint);
        return (String) this.get("endpoint");
    }

    public String getContentType() {
        return (String) this.get("contentType");
    }
    public String setContentType(String contentType) {
        this.put("contentType", contentType);
        return (String) this.get("contentType");
    }

    public Response getResponse() {
        return (Response)this.get("response");
    }
    public Response setResponse(Response response) {
        this.put("response", response);
        return (Response) this.get("response");
    }

    public RequestSpecification getRequest() {
        return (RequestSpecification)this.get("request");
    }
    public RequestSpecification setRequest(RequestSpecification request) {
        this.put("request", request);
        return (RequestSpecification) this.get("request");
    }
}

