package support;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigHelper {


    private static ConfigHelper instance;
    private Config conf;
    private static ClassLoader classLoader;

    protected ConfigHelper() {
        classLoader = getClass().getClassLoader();
        conf = ConfigFactory.load(classLoader, System
                .getProperty("project.properties", "project.properties"));
    }

    public static synchronized ConfigHelper getInstance() {
        if (instance == null) {
            instance = new ConfigHelper();
        }
        return instance;
    }

    public static String getString(String key) {
        return ConfigHelper.getInstance().conf.getString(key).replace("\"","").replace("'","");
    }

    public static String getCurrentWorkingDir() {

        Path currentRelativePath = Paths.get("");
        return currentRelativePath.toAbsolutePath().toString();

    }
}
